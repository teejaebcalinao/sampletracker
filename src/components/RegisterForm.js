import { useState } from 'react'
import { Button, Card, Col, Form } from 'react-bootstrap'
import { useHistory } from 'react-router'

export default function RegisterForm() {

    const history = useHistory()

    const [isLoading, setIsLoading] = useState(false)

    const [newUser, setnewUser] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: ""
    })

    const handleChange = e =>{
        setnewUser({
            ...newUser,
            [e.target.id] : e.target.value
        })
    }

    const handleSubmit= e =>{
        e.preventDefault()
        if(newUser.password.length < 8 || newUser.password !== newUser.confirmPassword) {
            alert("Password must be atleast 8 characters and should matched.")
        } else {
            setIsLoading(true)
            fetch(`${process.env.REACT_APP_BE_URL}/api/users`, {
                method: "POST",
                headers: {
                    'Content-Type' : 'application/json',
                },
                body: JSON.stringify(newUser)
            })
            .then(res => res.json())
            .then( data => {
                setIsLoading(false)
                if(!data.error){
                    alert("Registration successful. Please Login to continue")
                    history.push('/login')
                } else {
                    alert(data.error.message)
                }
            })
            .catch(err => console.log(err))
        }
    }

    return (
        <Card>
            <Card.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="firstName">
                        <Form.Label>First Name:</Form.Label>
                        <Form.Control type="text"  required  onChange={handleChange} value={newUser.firstName} />
                    </Form.Group>
                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name:</Form.Label>
                        <Form.Control type="text"  required  onChange={handleChange} value={newUser.lastName} />
                    </Form.Group>
                    <Form.Group controlId="email">
                        <Form.Label>Email:</Form.Label>
                        <Form.Control type="email"  required  onChange={handleChange} value={newUser.email} />
                    </Form.Group>
                    <Form.Row>
                        <Col>
                            <Form.Group controlId="password">
                                <Form.Label>Password:</Form.Label>
                                <Form.Control type="password"  required  onChange={handleChange} value={newUser.password} />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId="confirmPassword">
                                <Form.Label>Confirm Password:</Form.Label>
                                <Form.Control type="password"  required  onChange={handleChange} value={newUser.confirmPassword} />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    {
                        isLoading ? 
                        <Button type="submit" disabled>Regsiter</Button>:
                        <Button type="submit">Register</Button>

                    }
                </Form>
            </Card.Body>
        </Card>
    )
}
