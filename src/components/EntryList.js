import { useContext } from "react";
import { Table } from "react-bootstrap";
import { AppContext } from "../contexts/AppContext";
import EntryListtem from "./EntryListtem";


export default function EntryList({type}) {
    
    const {entries} = useContext(AppContext)
    
    const displayAllEntries = entries.filter(entry => entry.type === type && entry.category).map( entry =>{
        return <EntryListtem key={entry._id} entry={entry} />
    })

    const total = entries.filter(entry => entry.type === type).reduce( (x,y) => x + y.amount , 0)
    return (
        <Table className="text-center">
            <thead>
                <tr>
                    <th>Total</th>
                    <th> &#8369; {total.toFixed(2)}</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {displayAllEntries}
            </tbody>
        </Table>
    )
}
