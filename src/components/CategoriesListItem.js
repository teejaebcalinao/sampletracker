import { useContext, useState } from "react";
import { Card, Button, Modal,Form} from "react-bootstrap";
import { Pen, Trash } from "react-bootstrap-icons";
import { AppContext } from "../contexts/AppContext";

export default function CategoriesListItem({category}) {

    const { setLastUpdateCategory } = useContext(AppContext)
    const [isLoading, setIsLoading] = useState(false)
    const [show, setShow] = useState(false);

    const [selectedCategory, setSelectedCategory] = useState(category)
    const [type, setType] = useState(category.type)


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleChange = e => setSelectedCategory({...selectedCategory, [e.target.id] : e.target.value}) 
    const handleSubmit = e => {
        e.preventDefault()
        setIsLoading(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/categories/${category._id}`,{
            method: "PUT",
            headers: {
                "Authorization" : `Bearer ${localStorage["token"]}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({ 
                name: selectedCategory.name,
                type: type
            })
        })
        .then( res => res.json())
        .then( data =>{
            setIsLoading(false)
            if(!data.error) {
                alert("Category update successful")
                setLastUpdateCategory(data)
                handleClose()
            }
        })
    }

    const handleDelete = () =>{
        fetch(`${process.env.REACT_APP_BE_URL}/api/categories/${category._id}`,{
            method: "DELETE",
            headers: {
                "Authorization" : `Bearer ${localStorage["token"]}`
            }
        })
        .then( res => res.json())
        .then( data =>{
            if(!data.error) {
                alert("Delete category successful")
                setLastUpdateCategory(data)
                handleClose()
            }
        })
    }

    
    return (       
        <Card>
            <Card.Body>
                <Card.Title>
                    {category.name}
                </Card.Title>
                <Card.Text>
                    Type: {category.type}
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Button variant="outline-info" className="mx-1" onClick={handleShow}>
                    <Pen/>
                </Button>
                {
                    isLoading ?

                    <Button variant="outline-danger" className="mx-1" disabled>
                        <Trash/>
                    </Button>
                    :
                    <Button variant="outline-danger" className="mx-1" onClick={handleDelete}>
                        <Trash/>
                    </Button>
                }
            </Card.Footer>
            <Modal show={show} onHide={handleClose}>
                <Form onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                    <Modal.Title>Update Category</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group controlId="name">
                                <Form.Label>Name:</Form.Label>
                                <Form.Control type="text"  required value={selectedCategory.name} onChange={handleChange}/>
                            </Form.Group>
                            <Form.Control as="select" required value={type} onChange={e => setType(e.target.value)}>
                                <option>income</option>
                                <option>expense</option>
                            </Form.Control>
                    </Modal.Body>
                    <Modal.Footer>
                    
                    { isLoading ? <>
                        <Button variant="secondary" disabled>
                            Close
                        </Button>
                        <Button variant="primary" type="submit" disabled>
                            Save Changes
                        </Button>
                        </>
                        :<>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" type="submit">
                            Save Changes
                        </Button>
                        </>
                    }
                    </Modal.Footer>
                </Form>
            </Modal>
        </Card>
    )
}
